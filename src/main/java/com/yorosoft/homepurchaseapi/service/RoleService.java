package com.yorosoft.homepurchaseapi.service;

import com.yorosoft.homepurchaseapi.model.ERole;
import com.yorosoft.homepurchaseapi.model.Role;
import com.yorosoft.homepurchaseapi.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Role findByName(ERole s){
        Optional<Role> role = roleRepository.findByName(s);
        return role.orElseThrow(() -> new RuntimeException("Erreur: Role est introuvable."));
    }

}
