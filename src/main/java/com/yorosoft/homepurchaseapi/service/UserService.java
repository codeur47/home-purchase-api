package com.yorosoft.homepurchaseapi.service;

import com.yorosoft.homepurchaseapi.model.User;
import com.yorosoft.homepurchaseapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findByUsername(String s){
        Optional<User> user = userRepository.findByUsername(s);
        return user.orElseThrow(() -> new RuntimeException("Erreur: Utilisateur est introuvable."));
    }

    public Boolean existsByUsername(String s){
        return userRepository.existsByUsername(s);
    }

    public User save(User user){
        return userRepository.save(user);
    }

}
