package com.yorosoft.homepurchaseapi.repository;

import com.yorosoft.homepurchaseapi.model.ERole;
import com.yorosoft.homepurchaseapi.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
