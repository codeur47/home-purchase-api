package com.yorosoft.homepurchaseapi.payload.request;


import javax.validation.constraints.NotBlank;

public class LoginRequest {

    @NotBlank
    private String lastname;

    @NotBlank
    private String firstname;

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
