package com.yorosoft.homepurchaseapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomePurchaseApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomePurchaseApiApplication.class, args);
	}

}
