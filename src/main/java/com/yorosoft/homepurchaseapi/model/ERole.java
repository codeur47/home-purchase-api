package com.yorosoft.homepurchaseapi.model;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
